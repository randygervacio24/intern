import 'package:flutter/material.dart';
import 'package:intern_screen/constant.dart';

class internScreen extends StatefulWidget {
  const internScreen({Key? key}) : super(key: key);

  @override
  State<internScreen> createState() => _internScreenState();
}

class _internScreenState extends State<internScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        child: Scaffold(
          body: Column(
            children: [
              Text(Constant().kCompany,
              style: Constant().kTextBinagoStyle,),
              Text(Constant().internNames),
              Text(Constant().startingDate,
                  style: Constant().kTextBinagoStyle),
              Text(Constant().supervisorName,
                  style: Constant().kTextBinagoStyle),
              Text(Constant().dateToday,
                  style: Constant().kTextBinagoStyle),
            ],
          ),
        ),
      ),
    );
  }
}
