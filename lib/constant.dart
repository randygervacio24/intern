import 'package:flutter/material.dart';

class Constant {

  final kCompany= "XTreme Offshore Outsouring Inc.";
  final startingDate = "APRIL 17, 2023";
  final internNames = "Allen / Randy / Zahs / Enrique";
  final dateToday = "June 06, 2023";
  final supervisorName = "Jessua Pasoquin ft. Kobe";


  final kTextBinagoStyle = const TextStyle(
    fontSize: 30.0,
    color: Colors.pink,
    fontWeight: FontWeight.bold,
  );
}